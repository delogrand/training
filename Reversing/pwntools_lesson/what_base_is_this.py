"""
pwntools solution for what base is this? from pico 2018

"""

from pwn import *
import binascii

# totally didn't copy from stack overflow
#https://stackoverflow.com/questions/7396849/convert-binary-to-ascii-and-vice-versa
# b should be a list of strings of '1' and '0', so ['01010101']
def bits2string(b=None):
    return ''.join([chr(int(x, 2)) for x in b])


# establish an interface. This one is to a remote server
#   you can also use a local binary as the target for remote()
sh = remote('2018shell.picoctf.com', 14390)

# gives us binary, we have to make it a string (bin to ascii)
# processing/going through the recieved text until this stringself.
# this command does not save the string
sh.recvuntil('Please give me the ')
binary = sh.recvuntil(' as a word.', drop=True)
print('recieved: ' + str(binary))

bin_list = binary.split()
word = bits2string(bin_list)

print('sending: ' + str(word))
sh.sendline(word)

# now hex to word
sh.recvuntil('Please give me the ')
hex = sh.recvuntil(' as a word.', drop=True)
print('recieved: ' + str(hex))
hex = hex.decode() # turn the bytes literal to a string
word = bytearray.fromhex(hex).decode()
print('sending: ' + str(word))
sh.sendline(word)

# oct to word
sh.recvuntil('Please give me the  ') # there was an extra space
oct = sh.recvuntil(' as a word.', drop=True)
print('recieved: ' + str(oct))
oct = oct.decode() # turn the bytes literal to a string
word = ''.join([chr(int(oct_str_one_char, 8)) for oct_str_one_char in oct.split(' ')])
print('sending: ' + str(word))
sh.sendline(word)

print(sh.recvline(timeout=1))
print(sh.recvline(timeout=1))
print(sh.recvline(timeout=1))
print(sh.recvline(timeout=1))
print(sh.recvline(timeout=1))

# last one times out but we got it
# b'Flag: picoCTF{delusions_about_finding_values_602fd280}\n'


sh.close()
