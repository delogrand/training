# pwntools primer


## pwntools, what and why

Pwntools calls itself a " CTF framework and exploit development library." Its a python module that does the heavy lifting when you need to programmability interact with binaries both locally and remotely. Imagine a situation where you leak one address from a remote binary, and must use the leaked address and the relative position of another function in that library to find pwn a server. If you were to do leak the address and do the math yourself, you end up wasting time and risk the server just timing out. Pwntools can help by doing that math for you. Even if you are not a rev/pwn person, you still will run into problems that specifically require pwntools skills. These usually take the form of a series of questions that must be answered within a very short time frame.



## Installation

pwntools is available for python 3 and python 2. Take your pick. I will be using 3. If you get an error message about a 'lock' of some kind, run the command prepended with `sudo`.

### Python 3

Install with the following.

```bash
apt-get update
apt-get install python3 python3-pip python3-dev git libssl-dev libffi-dev build-essential
python3 -m pip install --upgrade pip
python3 -m pip install --upgrade pwntools
```

### Python 2

```bash
apt-get install python2.7 python2.7-dev python-pip
pip install pwntools
```

### Test Installation

Test out your installation by running a python file that just imports the module.

```bash
touch test_pwn.py
echo "from pwn import *" > test_pwn.py
python3 test_pwn.py
```

If you don't see anything, you did it right.



## Set up a connection or 'tube'

> "You need to talk to the challenge binary in order to pwn it, right?" - [sacred text of pwntools](https://docs.pwntools.com/en/stable/intro.html)

Connections in pwntools are called 'tubes.' There are 4 basic ways you can communicate with something you want to pwn.

* remote processes, like when you get a ip/domain and a port
* local binaries, downloaded executables, good for trying out your exploit because you can use a debugger on the executable
* listeners, when the thing you want to pwn reaches out to you
* ssh, set up an ssh connection

### remote processes

```
>>> conn = remote('ftp.ubuntu.com',21)
>>> conn.recvline() # doctest: +ELLIPSIS
b'220 ...'
>>> conn.send(b'USER anonymous\r\n')
>>> conn.recvuntil(b' ', drop=True)
b'331'
>>> conn.recvline()
b'Please specify the password.\r\n'
>>> conn.close()
```

### local binaries

```
>>> sh = process('/bin/sh')
>>> sh.sendline(b'sleep 3; echo hello world;')
>>> sh.recvline(timeout=1)
b''
>>> sh.recvline(timeout=5)
b'hello world\n'
>>> sh.close()
```

### listeners

```
>>> l = listen()
>>> r = remote('localhost', l.lport)
>>> c = l.wait_for_connection()
>>> r.send(b'hello')
>>> c.recv()
b'hello'
```

### ssh

Pwntools can also interact via ssh. There is a quick intro [here](https://docs.pwntools.com/en/stable/intro.html) and the full documentation on that is [here](https://docs.pwntools.com/en/stable/tubes/ssh.html#module-pwnlib.tubes.ssh).



## interacting with a connection

So you have a connection, now what? The simplist way to interact is by entering interactive mode. This acts like you just connected to the remote server with netcat or ran the binary from your console. You see what is printed to the screen and can send things by typing then and hitting enter. This is especially helpful after you get a shell, so you can ls and try and find the flag on their box.

```python
sh.interactive()
```

### programmatic interaction

Every byte you receive must be dealt with, even if it's as simple as just throwing away that byte. For example, take a program that works like this.

```
The magic number is 1337.
What is the magic number? 
```

The first step to most pwntools problems is getting your head around the problem. This usually means manually trying things or poking around for a bit. Say you want to use pwntools to pass the simple example above. You have connected to the server several times and the number is random so you can't just pipe in the argument. You know you just care about the number, so you have to get it from the text received. There are many different ways to go about this and pwntools has so many options in their [documentation](https://docs.pwntools.com/en/stable/tubes.html#pwnlib.tubes.tube.tube.recv), but I have found that `recvline()`, `recv()`, and `recvuntil()` are the most used.

```python
# tube is sh

# deal with the "The magic..." string
sh.recv(20) # receives 20 bytes but does not save them

# get our number
# the construct 'var = sh.any_recv()' saves the output in var
# recvuntil() receives data until the delim is reached. The delim here is the '.'
# if the optional argument drop=True is not included, then the delim is included
magic_number = sh.recvuntil('.', drop=True) # magic_num now has a string containing the bytes for 1337 as a string
```

Sending data is just as easy. I use `sendline()` most of the time. This sends the data that you provide as an argument and then a newline.

```python
# deal with "What is..."
sh.recvuntil('number? ')
sh.sendline(magic_number)
sh.interactive()
>>> Your flag is ctf{asdfasdfasdfasdf}
```



## setting the context

The context tells pwntools what it is you're dealing with. This can be linux or windows, little endian or big, or what assembly architecture you are dealing with.

```python
context.arch      = 'i386'
context.os        = 'linux'
context.endian    = 'little'
context.word_size = 32
```



## unpacking and packing

This is for when you need to stick data in 32 or 64 bits, or put it in a certain type of endianess. The [intro](https://docs.pwntools.com/en/stable/intro.html) covers some of this but there is more information in the full [documentation](https://docs.pwntools.com/en/stable/util/packing.html#module-pwnlib.util.packing).



## checksec

This useful tool tells you about the security settings on a binary (aslr, dep/nx).

```
checksec <executable>
# or
pwn checksec <executable>
```



## helpful pages, links, templates

### other people's guides

* very big pwntools guide, [link](https://github.com/Gallopsled/pwntools-tutorial)
* well written guide on start to finish exploiting a binary with pwntools. [link](https://tc.gts3.org/cs6265/2019/tut/tut03-02-pwntools.html)
* very shot cheat sheet, [link](https://tc.gts3.org/cs6265/2019/tut/tut03-02-pwntools.html)

### useful pages from pwntools docs

* pwntools docs on working with gdb, [link](https://docs.pwntools.com/en/stable/gdb.html)
* pwntools docs on working with tubes *very helpful*, [link](https://docs.pwntools.com/en/stable/tubes.html)
* pwntools docs on cyclic, which can tell you how much of an offset you need to overflow, [link](https://docs.pwntools.com/en/stable/util/cyclic.html)

### templates

Want a template? pwn has your back. Just use the pwn template command line tool. [docs](https://docs.pwntools.com/en/stable/commandline.html#pwn-template)

```
usage: pwn template [-h] [--host HOST] [--port PORT] [--user USER]
                    [--pass PASSWORD] [--path PATH] [--quiet]
                    [--color {never,always,auto}]
                    [exe]
```

**other templates**

* basic exploit template that can toggle between local and remote targets, [link](https://gist.github.com/grant-h/f47266a184fd65a79007aacc38ea4d60)
* rop template to give you an idea of what high tech stuff you can do with pwn [link](https://book.hacktricks.xyz/misc/basic-python/rop-pwn-template)
* 
